import re

from Dice import Dice


class DiceCombinationNotValidException(Exception):
    pass


class ZeroDieException(Exception):
    pass


class DiceCombination:

    def __init__(self, combination):
        quantity, number_of_faces, modifier = self.extract_combination(
            combination)

        if not quantity:
            raise ZeroDieException

        self.quantity = quantity

        self.dices = [
            Dice(number_of_faces)
            for n in range(self.quantity)
        ]

        if modifier is None:
            self.modifier = 0
        else:
            self.modifier = modifier

    def extract_combination(self, combination):

        dice_regex = re.compile(
            r'(?P<quantity>[0-9]+)d(?P<faces>[0-9]*)(?P<modifier>[+-][0-9]+)?')

        dice_search = dice_regex.search(combination)

        if dice_search:
            quantity, faces, modifier = dice_search.groups()

            return (
                int(quantity),
                int(faces) if faces != '' else None,
                int(modifier) if modifier else 0
            )

        raise DiceCombinationNotValidException

    def roll(self):

        results = [
            d.roll()
            for d in self.dices
        ]

        total = sum(results) + self.modifier

        return results, self.modifier, total
