from setuptools import setup, find_packages

setup(
    name="dyce",
    version='0.1',
    py_modules=['main'],
    packages=find_packages(),
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        dyce=scripts.main:cli
    ''',
)