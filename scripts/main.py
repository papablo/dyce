import click

from DiceCombination import DiceCombination


@click.group()
@click.option('--verbose', is_flag=True, help="Show more info")
@click.pass_context
def cli(ctx, verbose):
    pass

@cli.command()
@click.argument('combination')
@click.option('--verbose', is_flag=True, help="Show more info")
@click.pass_obj
def roll(ctx, verbose, combination):

    try:
        _d = DiceCombination(combination)

        results, modifier, total = _d.roll()

        click.echo(f'COMBINATION: {combination}')

        if verbose:
            click.echo(f'RESULTS: {results}')
            click.echo(f'MODIFIER: {modifier}')

        click.echo(f'TOTAL: {total}')

    except Exception as e:
        raise e
