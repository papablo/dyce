import random


class Dice:

    """
        Creates a D6 by default
    """

    def __init__(self, faces=6):

        if faces is None:
            self.faces = 6
        else:
            self.faces = faces
        
    def roll(self):
        return random.randint(1, self.faces)
